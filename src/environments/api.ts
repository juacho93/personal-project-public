/**
 * It contains the firebase's url
 */
 export const url = "https://identitytoolkit.googleapis.com/v1/accounts:"

 /**
  * It contains the apikey according to the firebase account
  */
 export const apiKey = "AIzaSyB45Smn04vR2t5v5oYiiD9eAwcJ05uEuik"
 
 /**
  * It contains the address to register
  */
 
  export const address1 = "signUp?key="
 
 
 /**
  * It contains the address to login
  */
 
  export const address2 = "signInWithPassword?key="

  /**
   * api's url
   */

  export const apiUrl = "https://login-angular-44578-default-rtdb.firebaseio.com"