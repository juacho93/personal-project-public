import { apiUrl } from './../../environments/api';
import { personModel } from './../models/personModel';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators' //Sirve para transformar loq ue un observable regresa

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  constructor(private http: HttpClient) { }

  insertPeople(person: personModel){
    return this.http.post(`${apiUrl}/people.json`,person)
               .pipe(
                 map((resp:any)=>{
                  person.id = resp.name;
                  return person;
                 })
               )
  }


  updatePerson(person: personModel){

    let tempPerson:any = {
      ...person

    };

    delete tempPerson.id

   return this.http.put(`${apiUrl}/people/${person.id}.json`,tempPerson)
  }


}
