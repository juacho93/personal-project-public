import { Product } from './../models/product';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartProductsService {

  products: Product[] = [
  
    new Product(1, 'Product 1', 'This is the product 1 description. The product is really good!', 100, 'https://c1-ebgames.eb-cdn.com.au/merchandising/images/packshots/811842bd0aa24025bcd2709e0ebd464e_Large.jpg'),
    new Product(2, 'Product 2', 'This is the product 2 description. The product is really good!', 150, 'https://c1-ebgames.eb-cdn.com.au/merchandising/images/packshots/811842bd0aa24025bcd2709e0ebd464e_Large.jpg'),
    new Product(3, 'Product 3', 'This is the product 3 description. The product is really good!', 50, 'https://c1-ebgames.eb-cdn.com.au/merchandising/images/packshots/811842bd0aa24025bcd2709e0ebd464e_Large.jpg'),
    new Product(4, 'Product 4', 'This is the product 4 description. The product is really good!', 200, 'https://c1-ebgames.eb-cdn.com.au/merchandising/images/packshots/811842bd0aa24025bcd2709e0ebd464e_Large.jpg'),
    new Product(5, 'Product 5', 'This is the product 5 description. The product is really good!', 100, 'https://c1-ebgames.eb-cdn.com.au/merchandising/images/packshots/811842bd0aa24025bcd2709e0ebd464e_Large.jpg'),
    new Product(6, 'Product 6', 'This is the product 6 description. The product is really good!', 250, 'https://c1-ebgames.eb-cdn.com.au/merchandising/images/packshots/811842bd0aa24025bcd2709e0ebd464e_Large.jpg'),
    new Product(7, 'Product 7', 'This is the product 7 description. The product is really good!', 80, 'https://c1-ebgames.eb-cdn.com.au/merchandising/images/packshots/811842bd0aa24025bcd2709e0ebd464e_Large.jpg'),
  ]

  constructor() { 
    
  }

  getProducts():Product[]{
   return this.products;
  }
}
