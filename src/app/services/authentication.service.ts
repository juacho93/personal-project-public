import { url, address2, apiKey, address1 } from './../../environments/api';
import { Injectable } from '@angular/core';
import { userModel } from '../models/userModel';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  userToken!: string;

 

  constructor(private http: HttpClient) {
    this.userToken
    this.leerToken()
   }

   logout(){
    localStorage.removeItem('token');
  }

  login(user: userModel){
    const authData = {
      ...user,
       returnSecureToken: true
     }

     return this.http.post(
      `${url}${address2}${apiKey}`,authData
    ).pipe(
      map(resp=>{
        this.saveToken(resp["idToken"])
        return resp
      })
    )
  }

  register(user: userModel){
    const authData = {
     ...user,
      returnSecureToken: true
    }

    return this.http.post(
      `${url}${address1}${apiKey}`,authData
    ).pipe(
      map(resp=>{
       this.saveToken(resp['idToken'])
        return resp
      })
    )

  }

  private saveToken(idToken: string){
    this.userToken = idToken
    localStorage.setItem('token', JSON.stringify(idToken))
    
  }

  //Lee el token
  leerToken(){
    if(localStorage.getItem('token')){
      this.userToken = JSON.parse(localStorage.getItem('token') || '{}')
    } else {
      this.userToken = ""
    }

    return this.userToken
  }


  isAuthenticated(): boolean{

    return this.userToken.length > 2
  }



}
