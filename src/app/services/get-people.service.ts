import { personModel } from './../models/personModel';
import { apiUrl } from './../../environments/api';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay, map } from 'rxjs/operators' //Sirve para transformar loque un observable regresa

@Injectable({
  providedIn: 'root'
})
export class GetPeopleService {

  constructor(private http: HttpClient) { }


  getPeople(){
    return this.http.get(`${apiUrl}/people.json`)
               .pipe(
                 map(this.createArray),
                 delay(1500)
                 );
  }

  private createArray(peopleObj: object){
   const people: personModel[] = []

   if(peopleObj === null){return [];}

   Object.keys(peopleObj).forEach(key =>{
     const person: personModel = peopleObj[key]
     person.id = key
     people.push(person);
   })
   return people
  }


  getPeopleById(id:string){
   return this.http.get(`${apiUrl}/people/${id}.json`)
  }

  deletePeople(id: string){
    return this.http.delete(`${apiUrl}/people/${id}.json`)
  }

}
