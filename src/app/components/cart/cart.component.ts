import { Product } from './../../models/product';
import { MessengerService } from './../../services/messenger.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  /**
   * Array which contains items of the products
   */
  cartItems: Array<{
    productId: number,
    productName: string,
    qty: number,  
    price: number
  }> = [];

  /**It initialezes the final total */
  cartTotal = 0

  /**
   * 
   * @param messengerService It contains the products
   */

  constructor(private messengerService: MessengerService) { }

  ngOnInit() {

    /**
     * It contains loads the products
     */
    this.messengerService.getMsg().subscribe((product: Product) =>{ 
      /**
       * It calls the fuction
       */
      this.addProductToCart(product)
     
    })

  }

  /**
   * It adds products to the cart
   */

  addProductToCart(product: Product){
    /**
     * We compare if the products exists
     */
    let productExist = false;

    /**
     * We traverse the cartItems to prove if there a similar product to overwrite it
     */

    for (let i in this.cartItems) {
      if(this.cartItems[i].productId === product.id){
        this.cartItems[i].qty++
        productExist = true;
        break;
      }
    }

    /**
     * We validate if the product does no exist, it adds to the cart
     */

    if(!productExist){
      this.cartItems.push({
        productId: product.id,
        productName: product.name,
        qty: 1,
        price: product.price
      });
    }

    /**
     * It initializes the total
     */
    this.cartTotal = 0

    /**
     * We traverse the cartItems and we add the cart total to multiply by the price
     */
    this.cartItems.forEach(item => {
      this.cartTotal += (item.qty * item.price)
    })
  }

  

}


