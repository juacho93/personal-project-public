import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent implements OnInit {

  /**
   * It gives the information to the other component
   */
  @Input() cartItems: any

  constructor() { }

  ngOnInit(): void {
  }

}
