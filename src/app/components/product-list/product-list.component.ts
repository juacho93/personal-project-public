import { Product } from './../../models/product';
import { CartProductsService } from './../../services/cart-products.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  /**
   * It saves the information into an array
   */
  productList: Product[] = []

  /**
   * 
   * @param cartProductsService It contains the products
   */

  constructor(private cartProductsService: CartProductsService) { }

  ngOnInit(): void {

    /**
     * It saves the data into an array
     */
    this.productList = this.cartProductsService.getProducts()
    console.log(this.productList)
       
  }



}
