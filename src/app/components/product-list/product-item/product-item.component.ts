import { MessengerService } from './../../../services/messenger.service';
import { Product } from './../../../models/product';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {

  /**
   * It gives the products to this component
   */
  @Input() productList:Product;

  /**
   * 
   * @param messengerService It contains the communication to another components
   */
  constructor(private messengerService: MessengerService) {
   
   }

  ngOnInit(): void {
  }

  /**
   * It adds products to the cart
   */
  handleAddToCart(){
    this.messengerService.sendMsg(this.productList)
  }

}
