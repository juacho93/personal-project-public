import { FormsEditComponent } from './pages/forms-edit/forms-edit.component';
import { AuthGuard } from './guards/auth.guard';
import { FormsComponent } from './pages/forms/forms.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShoppingCartComponent } from './pages/shopping-cart/shopping-cart.component';

const routes: Routes = [
  {path: "login", component: LoginComponent},
  {path: "register", component: RegisterComponent},
  {path: "dashboard", component: DashboardComponent, canActivate: [AuthGuard]},
  {path: "shopping", component: ShoppingCartComponent, canActivate: [AuthGuard]},
  {path: "forms", component: FormsComponent, canActivate: [AuthGuard]},
  {path: "forms-edit/:id", component: FormsEditComponent, canActivate: [AuthGuard]},
  {path: '',   redirectTo: '/login', pathMatch: 'full' },
  {path: '**', redirectTo: 'register' }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
