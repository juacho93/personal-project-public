import { AuthenticationService } from './../services/authentication.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor( private authentication: AuthenticationService,
    private router: Router) {}

  canActivate(): boolean {

    if ( this.authentication.isAuthenticated() ) {
      return true;
      } else {
      this.router.navigateByUrl('/login');
      return false;
      }

  }
  
}
