import { AuthenticationService } from './../../services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { userModel } from 'src/app/models/userModel';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  /**
   * Modelo para manejar los campos desde el HTML
   */

  user!: userModel;

 
/**
 * Constructor de la clase
 */
  constructor(private authentication: AuthenticationService,
              private router: Router) {
    this.user = new userModel
   }

  ngOnInit(): void {
    
  }

  /**
   * Metodo para registrar el logueo en el aplicativo
   */
  sendForm(f: NgForm){
    console.log(this.user)
    if(f.invalid){return}
    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere un momento'
     });
     Swal.showLoading();
    this.authentication.login(this.user)
   .subscribe(resp =>{
    Swal.close();
    this.router.navigateByUrl("/dashboard")
   console.log(resp)
  }, (err)=>{
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: err.error.error.message
    })
  })

    
   }
}
