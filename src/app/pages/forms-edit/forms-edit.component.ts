import { GetPeopleService } from './../../services/get-people.service';
import { PeopleService } from './../../services/people.service';
import { personModel } from './../../models/personModel';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2'
import { ActivatedRoute } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-forms-edit',
  templateUrl: './forms-edit.component.html',
  styleUrls: ['./forms-edit.component.scss']
})
export class FormsEditComponent implements OnInit {

  /**
   * Modelo para manejar el formulario
   */

  public person: personModel

  /**
   * Constructor de la clase
   * @param peopleService: Trae la data 
   * @param getPeopleService : Trae la data
   * @param activatedRoute : tare un id especifico
   */

  constructor(private peopleService: PeopleService, private getPeopleService: GetPeopleService, private activatedRoute: ActivatedRoute) { 
    this.person = new personModel
  }



  ngOnInit(): void {
    /**
     * Atrapa el id de cada registro
     */
    let id = this.activatedRoute.snapshot.paramMap.get('id') || ""
      
    /**
     * Validacion si se va a crear o se va a editar un registro
     */
  if(id !== "new"){
      this.getPeopleService.getPeopleById(id)
          .subscribe((resp: any) =>{
           this.person = resp
           this.person.id = id
           console.log(id)
           console.log(this.person)
          })
    }
  }

  /**
   * Metodo para almacernar un registro
   */
  saveInformation(f: NgForm){
   if(f.invalid){
     return;
   }


   if(this.person.id){
    this.peopleService.updatePerson(this.person).subscribe(resp =>{
      Swal.fire(
        `persona ${this.person.name} editada correctamente`,
        'presione okay para continuar',
        'success'
      )
       console.log(resp)
     })
   } else {
     this.peopleService.insertPeople(this.person).subscribe(resp =>{
      Swal.fire(
        `persona ${this.person.name} registrada correctamente`,
        'presione okay para continuar',
        'success'
      )
       console.log(resp)
     })
   }






    
  }


}
