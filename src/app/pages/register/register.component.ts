import { AuthenticationService } from './../../services/authentication.service';
import { userModel } from './../../models/userModel';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  /**
   * Modelo para manejar el registro de personas
   */
  user!: userModel;


/**
 * Constructor de la clase
 */
  constructor(private authentication: AuthenticationService,
              private router: Router) { 
    this.user = new userModel
  }

  ngOnInit(): void {
  }

  /**
   * Enviar el formulario para registrar una persona
   */
  sendForm(f: NgForm){
    console.log(this.user)
   if(f.invalid){return}
   Swal.fire({
    allowOutsideClick: false,
    icon: 'info',
    text: 'Espere un momento'
   });
   Swal.showLoading();
   this.authentication.register(this.user)
   .subscribe(resp =>{
    Swal.close();
    this.router.navigateByUrl("/dashboard")
   console.log(resp)
  }, (err)=>{
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: err.error.error.message
    })
  })

  }

}
