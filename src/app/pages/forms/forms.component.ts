import  Swal  from 'sweetalert2';
import { personModel } from './../../models/personModel';
import { GetPeopleService } from './../../services/get-people.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {

  /**
   * Modelo de tipo personModel para usar en el formulario
   */

  people: personModel[] = []

  /**
   * Muestra u oculta los loading
   */
  loading = false;

  /**
   * constructo de clase
   * @param getPeopleService: Trae el servicio para obtener la data
   */

  constructor(private getPeopleService: GetPeopleService) { }

  ngOnInit(): void {

    /**
     * Muestra la alerta de carga 
     */
    this.loading = true

    /**
     * Trae el servicio con la data
     */
    this.getPeopleService.getPeople()
        .subscribe(resp=>{
          this.people = resp
          this.loading = false
          console.log(this.people)
        })
  }


  /**
   * Elimina un registro
   */
  deletePeople(person: personModel, i:number){

    Swal.fire({
      title: `Estas segur@ de borrar a? ${person.name}`,
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((resp) => {
      if (resp.value) {

     this.people.splice(i, 1);

     this.getPeopleService.deletePeople(person.id)
       .subscribe()
       
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      }
    })

  }



}
